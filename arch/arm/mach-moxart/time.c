#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/io.h>
#include <linux/ioport.h>
#include <mach/hardware.h>
#include <mach/global_reg.h>
#include <asm/mach/time.h>

extern void moxart_int_set_irq(unsigned int irq, int mode, int level);

#define TIMER_1_COUNT(base_addr)		(base_addr + 0x00)
#define TIMER_1_LOAD(base_addr)			(base_addr + 0x04)
#define TIMER_1_MATCH1(base_addr)		(base_addr + 0x08)
#define TIMER_1_MATCH2(base_addr)		(base_addr + 0x0C)

#define TIMER_2_COUNT(base_addr)		(base_addr + 0x10)
#define TIMER_2_LOAD(base_addr)			(base_addr + 0x14)
#define TIMER_2_MATCH1(base_addr)		(base_addr + 0x18)
#define TIMER_2_MATCH2(base_addr)		(base_addr + 0x1C)

#define TIMER_3_COUNT(base_addr)		(base_addr + 0x20)
#define TIMER_3_LOAD(base_addr)			(base_addr + 0x24)
#define TIMER_3_MATCH1(base_addr)		(base_addr + 0x28)
#define TIMER_3_MATCH2(base_addr)		(base_addr + 0x2C)

#define TIMER_CR(base_addr)				(base_addr + 0x30)

#define TIMER_1_CR_ENABLE(base_addr)	(base_addr + 0x30)
#define TIMER_1_CR_EXTCLK(base_addr)	(base_addr + 0x34)
#define TIMER_1_CR_FLOWIN(base_addr)	(base_addr + 0x38)

#define TIMER_2_CR_ENABLE(base_addr)	(base_addr + 0x42)
#define TIMER_2_CR_EXTCLK(base_addr)	(base_addr + 0x46)
#define TIMER_2_CR_FLOWIN(base_addr)	(base_addr + 0x50)

#define TIMER_3_CR_ENABLE(base_addr)	(base_addr + 0x54)
#define TIMER_3_CR_EXTCLK(base_addr)	(base_addr + 0x58)
#define TIMER_3_CR_FLOWIN(base_addr)	(base_addr + 0x62)

#define TIMER_INTR_STATE(base_addr)		(base_addr + 0x34)


#define TIMEREG_1_CR_ENABLE			(1 << 0)
#define TIMEREG_1_CR_CLOCK			(1 << 1)
#define TIMEREG_1_CR_INT			(1 << 2)
#define TIMEREG_2_CR_ENABLE			(1 << 3)
#define TIMEREG_2_CR_CLOCK			(1 << 4)
#define TIMEREG_2_CR_INT			(1 << 5)
#define TIMEREG_3_CR_ENABLE			(1 << 6)
#define TIMEREG_3_CR_CLOCK			(1 << 7)
#define TIMEREG_3_CR_INT			(1 << 8)
#define TIMEREG_COUNT_UP			(1 << 9)
#define TIMEREG_COUNT_DOWN			(0 << 9)


#define APB_CLK		48000000
//#define APB_CLK		65000000
//#define APB_CLK		130000000
//#define AHB_CLK		96000000
#define MAX_TIMER	2
#define USED_TIMER	1
//#define SET_COUNTER	(APB_CLK / HZ)

#define TIMER1_COUNT                0x0
#define TIMER1_LOAD                 0x4
#define TIMER1_MATCH1               0x8
#define TIMER1_MATCH2               0xC
#define TIMER2_COUNT                0x10
#define TIMER2_LOAD                 0x14
#define TIMER2_MATCH1               0x18
#define TIMER2_MATCH2               0x1C
#define TIMER3_COUNT                0x20
#define TIMER3_LOAD                 0x24
#define TIMER3_MATCH1               0x28
#define TIMER3_MATCH2               0x2C 
#define TIMER_INTR_MASK		0x38

#define LEVEL                           0
#define EDGE                            1
#define H_ACTIVE                        0
#define L_ACTIVE                        1

typedef struct 
{
	unsigned int TimerValue;
	unsigned int TimerLoad;
	unsigned int TimerMatch1;
	unsigned int TimerMatch2;
} moxart_time_reg_t;

typedef struct
{       
	unsigned int Tm1En:1;		// Timer1 enable bit
	unsigned int Tm1Clock:1;	// Timer1 clock source (0: PCLK, 1: EXT1CLK)
	unsigned int Tm1OfEn:1;		// Timer1 over flow interrupt enable bit
	unsigned int Tm2En:1;
	unsigned int Tm2Clock:1;
	unsigned int Tm2OfEn:1;
	unsigned int Tm3En:1;
	unsigned int Tm3Clock:1;
	unsigned int Tm3OfEn:1;       
	unsigned int Reserved;        
} moxart_time_ctrl_t;

moxart_time_reg_t *TimerBase[] =
{
	0, 
	(moxart_time_reg_t *)IO_ADDRESS(MOXART_TIMER_BASE),
	(moxart_time_reg_t *)IO_ADDRESS(MOXART_TIMER_BASE)
};

static void moxart_timer_set_counter(int timer, unsigned int value)
{
	volatile moxart_time_reg_t *t = TimerBase[timer];    
	t->TimerValue = value; 
}

static void moxart_timer_set_reload(int timer, unsigned int value)
{
	volatile moxart_time_reg_t *t = TimerBase[timer];    
	t->TimerLoad = value;   
	t->TimerMatch1 = 0xffffffff;
	t->TimerMatch2 = 0xffffffff;
}

static int moxart_timer_enable(int timer)
{   
	moxart_time_ctrl_t *t=(moxart_time_ctrl_t *)(TIMER_CR(IO_ADDRESS(MOXART_TIMER_BASE)));

	if ((timer == 0) || (timer > MAX_TIMER)) return 0;

	switch(timer)
	{
		case 1:
			t->Tm1En=1;
			t->Tm1Clock = 0;	// use PCLK
			t->Tm1OfEn=1;
			break;    
		case 2:
			t->Tm2En=1;
			t->Tm2Clock = 0;	// use PCLK
			t->Tm2OfEn=1;
			break; 
		case 3:
			t->Tm3En=1;
			t->Tm3Clock = 0;	// use PCLK
			t->Tm3OfEn=1;
			break; 
		default:
			break;   
	}
	return 1;
}

static unsigned long moxart_timer_get_counter(int timer)
{
	volatile moxart_time_reg_t *t = TimerBase[timer];
	return (volatile unsigned long)(t->TimerValue);
}

unsigned long moxart_gettimeoffset (void)
{
	unsigned long offsetticks;

	offsetticks = moxart_timer_get_counter(USED_TIMER);
	offsetticks = (APB_CLK / HZ) - offsetticks;
	if ( *(volatile unsigned int *)(TIMER_INTR_STATE(IO_ADDRESS(MOXART_TIMER1_BASE))) ) {
		//printk("[has timer interrupt pending , %d !]\n", offsetticks);
		offsetticks = moxart_timer_get_counter(USED_TIMER);
		offsetticks = (APB_CLK / HZ) - offsetticks;
		offsetticks += (APB_CLK / HZ);
	}
	offsetticks = offsetticks / (APB_CLK / 1000000);	// tansfer ticks to usec
	return offsetticks;
}

/* IRQ handler for the timer */
static irqreturn_t moxart_timer_interrupt(int irq, void *dev_id)
{
	//unsigned int t_int;
	timer_tick();
	//printk("TIMER moxart_timer_interrupt irq = %d\n", irq);
	//*(volatile unsigned int *)(TIMER_INTR_STATE(IO_ADDRESS(MOXART_TIMER1_BASE))) = 0;
	__raw_writel(0, TIMER_INTR_STATE(IO_ADDRESS(MOXART_TIMER1_BASE)));
	return IRQ_HANDLED;
}

static struct irqaction moxart_timer_irq = {
	.name		= "MOXART Timer Tick",
	.flags		= IRQF_DISABLED | IRQF_TIMER,
	.handler	= moxart_timer_interrupt,
};

static irqreturn_t gemini_timer_interrupt(int irq, void *dev_id)
{
	timer_tick();
	return IRQ_HANDLED;
}

static struct irqaction gemini_timer_irq = {
	.name		= "Gemini Timer Tick",
	.flags		= IRQF_DISABLED | IRQF_TIMER,
	.handler	= gemini_timer_interrupt,
};

static struct resource timer_resource = {
	.name	= "timer_handler",
	.start	= IO_ADDRESS(MOXART_TIMER_BASE),
	.end	= IO_ADDRESS(TIMER_INTR_STATE(MOXART_TIMER_BASE)) + 4,
};

/*void __init moxart_timer_init(void)*/
void moxart_timer_init(void)
{
	unsigned int t_int;
	int io_resource;
	/*unsigned int tick_rate, reg_v;
	reg_v = __raw_readl(IO_ADDRESS((MOXART_AHB_BASE + GLOBAL_STATUS)));
	tick_rate = REG_TO_AHB_SPEED(reg_v) * 1000000;
	printk(KERN_INFO "Bus: %dMHz", tick_rate / 1000000); // Bus: 130MHz
	tick_rate /= 6;		// APB bus run AHB*(1/6)
	tick_rate /= 2;
	switch(reg_v & CPU_AHB_RATIO_MASK) {
		case CPU_AHB_1_1:
			printk(KERN_CONT " (1/1)\n");
			break;
		case CPU_AHB_3_2:
			printk(KERN_CONT " (3/2)\n");
			break;
		case CPU_AHB_24_13:
			printk(KERN_CONT " (24/13)\n");
			break;
		case CPU_AHB_2_1:
			printk(KERN_CONT " (2/1)\n");
			break;
	}*/

	/*moxart_timer_set_reload(1, APB_CLK/HZ);
	moxart_timer_set_counter(1, APB_CLK/HZ);
	if( !moxart_timer_enable(1) ) {
		panic("can not enable timer\n");
	}
	printk("IRQ timer at interrupt number 0x%x clock %d\r\n", IRQ_TIMER1, APB_CLK);
	moxart_int_set_irq(IRQ_TIMER1, EDGE, L_ACTIVE);
	setup_irq(IRQ_TIMER1, &moxart_timer_irq);*/

	//io_resource = request_resource(&iomem_resource, &timer_resource);

	//moxart_int_set_irq(IRQ_TIMER1, 1, 1);
	setup_irq(IRQ_TIMER1, &moxart_timer_irq);
	//request_irq(IRQ_TIMER1, &gemini_timer_interrupt, IRQF_DISABLED | IRQF_TIMER, "MOXART TIMER", 1010);

	/*t_int = __raw_readl(TIMER_INTR_STATE(IO_ADDRESS(MOXART_TIMER1_BASE)));
	printk("TIMER TIMER_INTR_STATE(IO_ADDRESS(MOXART_TIMER1_BASE)) = %d\n", t_int);
	t_int = __raw_readl(TIMER_1_COUNT(IO_ADDRESS(MOXART_TIMER1_BASE)));
	printk("TIMER TIMER_1_COUNT(IO_ADDRESS(MOXART_TIMER1_BASE)) = %d\n", t_int);*/

	unsigned int tick_rate = 48000000;
	__raw_writel(tick_rate / HZ, TIMER_1_COUNT(IO_ADDRESS(MOXART_TIMER_BASE)));
	__raw_writel(tick_rate / HZ, TIMER_1_LOAD(IO_ADDRESS(MOXART_TIMER_BASE)));

	/*__raw_writel(0xffffffff, TIMER_1_MATCH1(IO_ADDRESS(MOXART_TIMER_BASE)));
	__raw_writel(0xffffffff, TIMER_1_MATCH2(IO_ADDRESS(MOXART_TIMER_BASE)));*/

	//__raw_writel(TIMER_1_CR_ENABLE | TIMER_1_CR_INT, TIMER_CR(IO_ADDRESS(MOXART_TIMER1_BASE)));
	//__raw_writel((1 << 0) | (1 << 2), TIMER_1_CR_ENABLE(IO_ADDRESS(MOXART_TIMER_BASE)));
	//__raw_writel(0x0000020f, TIMER_1_CR_ENABLE(IO_ADDRESS(MOXART_TIMER_BASE)));
	//__raw_writel(TIMEREG_COUNT_UP | TIMEREG_1_CR_ENABLE | TIMEREG_1_CR_INT | TIMEREG_1_CR_CLOCK, TIMER_1_CR_ENABLE(IO_ADDRESS(MOXART_TIMER_BASE)));

	//__raw_writel(TIMEREG_COUNT_UP | TIMEREG_1_CR_ENABLE | TIMEREG_1_CR_INT, TIMER_1_CR_ENABLE(IO_ADDRESS(MOXART_TIMER_BASE)));

	__raw_writel(1, TIMER_1_CR_ENABLE(IO_ADDRESS(MOXART_TIMER_BASE)));
	__raw_writel(0, TIMER_1_CR_EXTCLK(IO_ADDRESS(MOXART_TIMER_BASE)));
	__raw_writel(1, TIMER_1_CR_FLOWIN(IO_ADDRESS(MOXART_TIMER_BASE)));

	/*t_int = __raw_readl(TIMER_INTR_STATE(IO_ADDRESS(MOXART_TIMER1_BASE)));
	printk("TIMER TIMER_INTR_STATE(IO_ADDRESS(MOXART_TIMER1_BASE)) = %d\n", t_int);*/

	t_int = __raw_readl(TIMER_1_MATCH1(IO_ADDRESS(MOXART_TIMER1_BASE)));
	printk("TIMER TIMER_1_MATCH1(IO_ADDRESS(MOXART_TIMER1_BASE)) = %d\n", t_int);
	t_int = __raw_readl(TIMER_1_MATCH2(IO_ADDRESS(MOXART_TIMER1_BASE)));
	printk("TIMER TIMER_1_MATCH2(IO_ADDRESS(MOXART_TIMER1_BASE)) = %d\n", t_int);

	t_int = __raw_readl(TIMER_1_COUNT(IO_ADDRESS(MOXART_TIMER1_BASE)));
	printk("TIMER TIMER_1_COUNT(IO_ADDRESS(MOXART_TIMER1_BASE)) = %d\n", t_int);
	t_int = __raw_readl(TIMER_1_LOAD(IO_ADDRESS(MOXART_TIMER1_BASE)));
	printk("TIMER TIMER_1_LOAD(IO_ADDRESS(MOXART_TIMER1_BASE)) = %d\n", t_int);

	t_int = __raw_readl(TIMER_1_COUNT(IO_ADDRESS(MOXART_TIMER1_BASE)));
	printk("TIMER TIMER_1_COUNT(IO_ADDRESS(MOXART_TIMER1_BASE)) = %d\n", t_int);
	t_int = __raw_readl(TIMER_1_LOAD(IO_ADDRESS(MOXART_TIMER1_BASE)));
	printk("TIMER TIMER_1_LOAD(IO_ADDRESS(MOXART_TIMER1_BASE)) = %d\n", t_int);

	//release_resource(io_resource);
	printk("TIMER finished setting up timer\n");
}

struct sys_timer moxart_timer = {
    .init       = moxart_timer_init,
//    .offset     = moxart_gettimeoffset,
};

