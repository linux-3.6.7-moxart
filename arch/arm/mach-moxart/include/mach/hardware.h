/*
 *  This file contains the hardware definitions for Gemini.
 *
 *  Copyright (C) 2001-2006 Storlink, Corp.
 *  Copyright (C) 2008-2009 Paulius Zaleckas <paulius.zaleckas@teltonika.lt>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
#ifndef __MACH_HARDWARE_H
#define __MACH_HARDWARE_H

#define MOXART_AHB_BASE			0x90100000
#define MOXART_UART_BASE		0x98200000
#define MOXART_UART1_BASE		0x98200000
#define MOXART_UART2_BASE		0x98200020
#define MOXART_TIMER_BASE		0x98400000
#define MOXART_TIMER1_BASE		MOXART_TIMER_BASE
#define MOXART_TIMER2_BASE		(MOXART_TIMER_BASE + 0x10)
#define MOXART_RTC_BASE			0x98600000
#define MOXART_INTERRUPT_BASE	0x98800000
#define MOXART_FLASH_BASE		0x80000000

#define MOXART_FLASH_VA_BASE	0xf4000000

/*#define MOXART_WATCHDOG_BASE		0x98500000

#define MOXART_GLOBAL_BASE	0x90100000
#define MOXART_POWER_CTRL_BASE	0x4B000000
#define MOXART_GMAC0_BASE	0x6000A000
#define MOXART_GMAC1_BASE	0x6000E000
#define MOXART_FLASH_CTRL_BASE	0x65000000
#define MOXART_DRAM_CTRL_BASE	0x66000000
#define MOXART_GENERAL_DMA_BASE	0x67000000*/

#define UART_CLK	14745600 

/*
 * macro to get at IO space when running virtually
 */

/* #define IO_ADDRESS(x)	((((x) & 0xFFF00000) >> 4) | ((x) & 0x000FFFFF) | 0xF0000000)*/

#define IO_BASE	                        0xf0000000
#define MEM_ADDRESS(x)                  ((x&0x0fffffff)+IO_BASE)
#define IO_ADDRESS(x)                   (((x>>4)&0xffff0000)+(x&0xffff)+IO_BASE) 
#define PHY_ADDRESS(x)                  (((x<<4)&0xfff00000)+(x&0xffff))



/* Following is used Moxa CPU demo board
 * Physical     Logical
 * 90100000	f9010000	AHB controller
 * 90200000	f9020000	SMC
 * 90300000	f9030000	SDRAM controller
 * 90400000	f9040000	DMA
 * 90500000	f9050000	APB bridge
 * 90900000	f9090000	MAC #1
 * 90a00000	f90a0000	USB 2.0 host
 * 90b00000	f90b0000	USB 2.0 device
 * 90c00000	f90c0000	PCI bridge
 * 90f00000	f90f0000	DES/3DES/AES encryptor
 * 92000000	f9200000	MAC #2
 * 92300000	f9230000	EBI
 * 98100000	f9810000	PMU (power management)
 * 98200000	f9820000	UART (port 1 - 6), embedded on CPU
 * 98400000	f9840000	timer #1 & #2
 * 98500000	f9850000	watchdog timer
 * 98600000	f9860000	RTC, embedded on CPU
 * 98700000	f9870000	GPIO
 * 98800000	f9880000	INTC (interrupt controller)
 * 98b00000	f98b0000	SPI
 * 98e00000	f98e0000	SD controller
 * 99400000	f9940000	AC97
 * a0000000	a0000000	PCI memory
 */


#endif
