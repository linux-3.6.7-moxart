/*
 *  Copyright (C) 2001-2006 Storlink, Corp.
 *  Copyright (C) 2008-2009 Paulius Zaleckas <paulius.zaleckas@teltonika.lt>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
#ifndef __MACH_MEMORY_H
#define __MACH_MEMORY_H

#define DRAM_BASE       0x00000000
#define DRAM_SIZE       SZ_32M
#define MEM_SIZE        DRAM_SIZE

#define PHYS_OFFSET     UL(DRAM_BASE)
#define END_MEM         (DRAM_BASE + DRAM_SIZE)

#endif /* __MACH_MEMORY_H */
