#include <linux/init.h>
#include <linux/io.h>
#include <linux/ioport.h>
#include <linux/stddef.h>
#include <linux/list.h>
#include <linux/sched.h>
#include <asm/irq.h>
#include <asm/mach/irq.h>
#include <mach/hardware.h>

#define IRQ_SOURCE(base_addr)	(base_addr + 0x00)
#define IRQ_MASK(base_addr)		(base_addr + 0x04)
#define IRQ_CLEAR(base_addr)	(base_addr + 0x08)
#define IRQ_TMODE(base_addr)	(base_addr + 0x0C)
#define IRQ_TLEVEL(base_addr)	(base_addr + 0x10)
#define IRQ_STATUS(base_addr)	(base_addr + 0x14)
#define FIQ_SOURCE(base_addr)	(base_addr + 0x20)
#define FIQ_MASK(base_addr)		(base_addr + 0x24)
#define FIQ_CLEAR(base_addr)	(base_addr + 0x28)
#define FIQ_TMODE(base_addr)	(base_addr + 0x2C)
#define FIQ_TLEVEL(base_addr)	(base_addr + 0x30)
#define FIQ_STATUS(base_addr)	(base_addr + 0x34)

void moxart_generic_set_generic(unsigned int base, unsigned int irq, unsigned int low)
{
	unsigned int mask;
	mask = __raw_readl(base);
	( low ) ? ( mask |= (1 << irq) ) : ( mask &= ~(1 << irq) );
	__raw_writel(mask, base);
}

/*void __init moxart_int_set_irq(unsigned int irq, int mode, int level)*/
void moxart_int_set_irq(unsigned int irq, int mode, int level)
{
	unsigned long flags;
#ifdef USE_SPINLOCK
	spin_lock_irqsave(&moxart_int_lock, flags);
#endif
	if (irq < 32) {
		moxart_generic_set_generic(IRQ_TMODE(IO_ADDRESS(MOXART_INTERRUPT_BASE)), irq, mode);
		moxart_generic_set_generic(IRQ_TLEVEL(IO_ADDRESS(MOXART_INTERRUPT_BASE)), irq, level);
		goto moxart_int_set_irq_exit;
	}
	if (irq < 64) {
		irq -= 32;
		moxart_generic_set_generic(FIQ_TMODE(IO_ADDRESS(MOXART_INTERRUPT_BASE)), irq, mode);
		moxart_generic_set_generic(FIQ_TLEVEL(IO_ADDRESS(MOXART_INTERRUPT_BASE)), irq, level);
		goto moxart_int_set_irq_exit;
	}
moxart_int_set_irq_exit:
	if(1) {}
#ifdef USE_SPINLOCK
	spin_unlock_irqrestore(&moxart_int_lock, flags);
#endif
}

static void gemini_ack_irq(struct irq_data *d)
{
	unsigned int irq = d->irq;
	if (irq < 32) {
		__raw_writel(1 << irq, IRQ_CLEAR(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	}
	if (irq < 64) {
		irq -= 32;
		__raw_writel(1 << irq, FIQ_CLEAR(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	}
}

static void gemini_mask_irq(struct irq_data *d)
{
	unsigned int mask;
	unsigned int irq = d->irq;
	if (irq < 32) {
		mask = __raw_readl(IRQ_MASK(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
		mask &= ~(1 << irq);
		__raw_writel(mask, IRQ_MASK(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	}
	if (irq < 64) {
		irq -= 32;
		mask = __raw_readl(FIQ_MASK(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
		mask &= ~(1 << irq);
		__raw_writel(mask, FIQ_MASK(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	}
}

static void gemini_unmask_irq(struct irq_data *d)
{
	unsigned int mask;
	unsigned int irq = d->irq;
	if (irq < 32) {
		mask = __raw_readl(IRQ_MASK(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
		mask |= (1 << irq);
		__raw_writel(mask, IRQ_MASK(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	}
	if (irq < 64) {
		irq -= 32;
		mask = __raw_readl(FIQ_MASK(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
		mask |= (1 << irq);
		__raw_writel(mask, FIQ_MASK(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	}
}

static struct irq_chip gemini_irq_chip = {
	.name	= "INTC",
	.irq_ack	= gemini_ack_irq,
	.irq_mask	= gemini_mask_irq,
	.irq_unmask	= gemini_unmask_irq,
};

static struct resource irq_resource = {
	.name	= "irq_handler",
	.start	= IO_ADDRESS(MOXART_INTERRUPT_BASE),
	.end	= IO_ADDRESS(FIQ_STATUS(MOXART_INTERRUPT_BASE)) + 4,
};

/*static __init void moxart_int_init(void)
{
	spin_lock_init(&moxart_int_lock);

	//init interrupt controller
	__raw_writel(0, IRQ_MASK(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	__raw_writel(0, FIQ_MASK(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	__raw_writel(0xffffffff, IRQ_CLEAR(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	__raw_writel(0xffffffff, FIQ_CLEAR(IO_ADDRESS(MOXART_INTERRUPT_BASE)));

}*/

/*void __init moxart_init_irq(void)*/
void moxart_init_irq(void)
{
	unsigned long flags, flags2;
	unsigned int mode = 0, level = 0, irq;
	int io_resource;

	/*local_irq_disable();
	local_fiq_disable();*/

	//disable_hlt();

	io_resource = request_resource(&iomem_resource, &irq_resource);

	/*local_irq_save(flags); // UP no longer provided (use SMP) save_and_cli(flags);
	spin_lock_init(&moxart_int_lock);*/


	for (irq = 0; irq < NR_IRQS; irq++) {
		irq_set_chip(irq, &gemini_irq_chip);
		if(irq == IRQ_TIMER1 || irq == IRQ_TIMER2 || irq == IRQ_TIMER3 /*|| irq == IRQ_UART*/) {
			irq_set_handler(irq, handle_edge_irq);
			//set_irq_handler(irq, handle_level_irq);
			mode |= (1 << irq);
			level |= (1 << irq);
		}
		else {
			irq_set_handler(irq, handle_level_irq);
		}
		set_irq_flags(irq, IRQF_VALID | IRQF_PROBE);
		//set_irq_flags(irq, IRQF_VALID);
	}
	printk("IRQ chip/handler/flags assign finished\r\n");

//	local_irq_save(flags2);
#ifdef USE_SPINLOCK
	spin_lock_init(&moxart_int_lock);
	spin_lock_irqsave(&moxart_int_lock, flags); // UP no longer provided (use SMP) save_and_cli(flags);
#endif

	__raw_writel(0, IRQ_MASK(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	__raw_writel(0, FIQ_MASK(IO_ADDRESS(MOXART_INTERRUPT_BASE)));

	__raw_writel(0xffffffff, IRQ_CLEAR(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	__raw_writel(0xffffffff, FIQ_CLEAR(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	printk("IRQ init finished\r\n");

	__raw_writel(mode, IRQ_TMODE(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	__raw_writel(level, IRQ_TLEVEL(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	__raw_writel(0, FIQ_TMODE(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	__raw_writel(0, FIQ_TLEVEL(IO_ADDRESS(MOXART_INTERRUPT_BASE)));
	printk("IRQ mode/level setup finished\r\n");

#ifdef USE_SPINLOCK
	spin_unlock_irqrestore(&moxart_int_lock, flags); // UP no longer provided (use SMP) restore_flags(flags);
#endif
//	local_irq_restore(flags2);

	//moxart_int_set_irq(IRQ_TIMER1, 1, 1);

	//local_irq_restore(flags); // UP no longer provided (use SMP) restore_flags(flags);

	/*local_irq_enable();
	local_fiq_enable();*/

	//release_resource(io_resource);

	printk("IRQ moxart_init_irq finished\r\n");
}
