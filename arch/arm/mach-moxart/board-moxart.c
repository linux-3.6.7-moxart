#include <linux/kernel.h>
#include <linux/mm.h>
#include <linux/init.h>

#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/time.h>
#include <asm/mach/map.h>

#include <linux/serial_8250.h>
#include <mach/hardware.h>
#include <mach/irqs.h>
#include <linux/mtd/physmap.h>

extern void moxart_init_irq(void);
//extern void moxart_timer_init(void);
//extern unsigned long moxart_gettimeoffset(void);
extern struct sys_timer moxart_timer;

static struct uart_port moxa_serial_ports[] = {
	{
		.iobase		= IO_ADDRESS(MOXART_UART1_BASE),
		.mapbase	= MOXART_UART1_BASE,
		.irq		= IRQ_UART,
		.flags		= UPF_SKIP_TEST | UPF_SHARE_IRQ,
		.iotype		= UPIO_PORT,	
		.regshift	= 2,
		.uartclk	= UART_CLK,
		.line		= 0,
		.type		= PORT_16550A,
		.fifosize	= 16
	},
	{
		.iobase		= IO_ADDRESS(MOXART_UART2_BASE),
		.mapbase	= MOXART_UART2_BASE,
		.irq		= IRQ_UART,
		.flags		= UPF_SKIP_TEST | UPF_SHARE_IRQ,
		.iotype		= UPIO_PORT,	
		.regshift	= 2,
		.uartclk	= UART_CLK,
		.line		= 1,
		.type		= PORT_16550A,
		.fifosize	= 16
	}
};

/*static struct platform_device serial_device = {
	.name	= "serial8250",
	.id	= PLAT8250_DEV_PLATFORM,
	.dev	= {
		.platform_data = moxa_serial_ports,
	},
};

void platform_register_uart(void)
{
	platform_device_register(&serial_device);
}*/

static void __init moxa_serial_init(void)
{
	early_serial_setup(&moxa_serial_ports[0]);
	early_serial_setup(&moxa_serial_ports[1]);
}

static void __init fixup_uc711x(struct machine_desc *desc, struct tag *tags, char **cmdline, struct meminfo *mi) 
{
	moxa_serial_init();
}

/* Page table mapping for I/O region */
static struct map_desc moxart_io_desc[] __initdata = {
	{
		.virtual	= IO_ADDRESS(MOXART_AHB_BASE),
		.pfn		=__phys_to_pfn(MOXART_AHB_BASE),
		.length		= SZ_4K,
		.type 		= MT_DEVICE,
	}, {
		.virtual	= IO_ADDRESS(MOXART_TIMER_BASE),
		.pfn		= __phys_to_pfn(MOXART_TIMER_BASE),
		.length		= SZ_4K,
		.type 		= MT_DEVICE,
    }, {
		.virtual	= IO_ADDRESS(MOXART_RTC_BASE),
		.pfn		= __phys_to_pfn(MOXART_RTC_BASE),
		.length		= SZ_4K,
		.type 		= MT_DEVICE,
	}, {
        .virtual    = IO_ADDRESS(MOXART_UART_BASE),
        .pfn        = __phys_to_pfn(MOXART_UART_BASE),
        .length     = SZ_4K,
        .type       = MT_DEVICE,
	}, {
		.virtual    = IO_ADDRESS(MOXART_INTERRUPT_BASE),
		.pfn        = __phys_to_pfn(MOXART_INTERRUPT_BASE),
		.length     = SZ_4K,
		.type       = MT_DEVICE,
	}, {
		.virtual    = IO_ADDRESS(MOXART_FLASH_BASE),
		.pfn        = __phys_to_pfn(MOXART_FLASH_BASE),
		.length     = SZ_16M,
		.type       = MT_DEVICE,
    }
};

static void __init moxart_map_io(void)
{
    iotable_init(moxart_io_desc, ARRAY_SIZE(moxart_io_desc));
}

/*void __init moxart_init_irq(void)
{
}*/

/*
unsigned long cpe_gettimeoffset (void)
{
    unsigned long offsetticks = 0;
    return offsetticks;
}
void __init moxart_timer_init(void)
{
}
*/

static void __init moxart_init(void)
{
	//platform_register_uart();
	//platform_register_pflash(SZ_16M, NULL, 0);
}

MACHINE_START(MOXART, "Botech MOXA")
	.fixup = fixup_uc711x,
//	.boot_params = 0x100,
//	.atag_offset	= 0x100,
//	.param_offset   = 0x00000100,
	.map_io		= moxart_map_io,
	.init_machine	= moxart_init,
	.init_irq	= moxart_init_irq,
	.timer		= &moxart_timer,
MACHINE_END


